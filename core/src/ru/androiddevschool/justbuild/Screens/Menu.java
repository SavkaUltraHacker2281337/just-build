package ru.androiddevschool.justbuild.Screens;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import ru.androiddevschool.justbuild.Controller.ScreenTraveler;
import ru.androiddevschool.justbuild.Utils.Assets;

import static ru.androiddevschool.justbuild.Utils.Names.ScreenName.PLAY;


/**
 * Created by 06k1102 on 30.03.2017.
 */
public class Menu extends StdScreen {

    public Menu(SpriteBatch batch) {
        super(batch);
    }
    protected void initBg(Stage stage){
        stage.addActor(new Image(Assets.get().drawable("ui/bg")));
    }
    protected void initWorld(Stage stage){}
    protected void initUi(Stage stage){
        Table table = new Table();
        table.setFillParent(true);

        Button button = new TextButton("Play", (TextButton.TextButtonStyle) Assets.get().btnStyles.get("long-text"));
        button.addListener(new ScreenTraveler(PLAY));
        table.add(button);
        stage.addActor(table);
    }
}



