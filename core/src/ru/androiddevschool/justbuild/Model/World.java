package ru.androiddevschool.justbuild.Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.Viewport;

import ru.androiddevschool.justbuild.Utils.Assets;
import ru.androiddevschool.justbuild.Utils.Values;

/**
 * Created by 06k1102 on 06.04.2017.
 */
public class World extends Stage {
    Block current;
    Block previous;
    float startHeight;
    float levelHeight;
    float currentlevel;
    Actor camCenter;

    public World(Viewport viewport, Batch batch) {
        super(viewport, batch);
        previous = new Block(Assets.get().drawable("ui/start-blooock"));
        previous.setPosition(Values.WORLD_WIDTH / 2 - previous.getWidth() / 2, 0);
        previous.stop();
        addActor(previous);

        current = new Block(Assets.get().drawable("ui/myblock"));
        current.setPosition(0, previous.getTop() + 3*current.getHeight());
        addActor(current);

        currentlevel = 0;
        startHeight = previous.getHeight();
        levelHeight = current.getHeight();
        camCenter = new Actor();
        camCenter.setPosition(Values.WORLD_WIDTH/2, startHeight + 3*levelHeight);
        addActor(camCenter);
    }

    public void act(float delta){
        super.act(delta);
        getCamera().position.x = camCenter.getX();
        getCamera().position.y = camCenter.getY();
    }

    public boolean touchDown (int screenX, int screenY, int pointer, int button) {
        tap();
        return true;
    }

    public void tap() {
        current.drop(previous);
        if(!current.moving) {
            currentlevel++;
            previous = current;
            current = new Block(Assets.get().drawable("ui/myblock"));
            current.setPosition(0, startHeight + (currentlevel + 3) * levelHeight);
            addActor(current);
            camCenter.addAction(Actions.moveBy(0, levelHeight, 0.5f));
        }
    }
}
