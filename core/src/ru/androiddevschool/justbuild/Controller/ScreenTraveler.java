package ru.androiddevschool.justbuild.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import ru.androiddevschool.justbuild.JustBuild;
import ru.androiddevschool.justbuild.Utils.Assets;
import ru.androiddevschool.justbuild.Utils.Names;

/**
 * Created by 06k1102 on 06.04.2017.
 */
public class ScreenTraveler extends ClickListener implements Names {
    private ScreenName name;

    public ScreenTraveler(ScreenName name) {
        super();
        this.name = name;
    }

    public void clicked(InputEvent event, float x, float y) {
        Assets.get().sounds.get("hit").play();
        JustBuild.get().setScreen(name);
    }
}
