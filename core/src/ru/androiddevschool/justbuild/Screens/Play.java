package ru.androiddevschool.justbuild.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import ru.androiddevschool.justbuild.Model.World;
import ru.androiddevschool.justbuild.Utils.Assets;

import static ru.androiddevschool.justbuild.Utils.Names.StageName.WORLD;

/**
 * Created by 06k1102 on 06.04.2017.
 */
public class Play extends StdScreen {
    private World world;
    public Play(SpriteBatch batch) {
        super(batch);
    }
    public void show(){
        super.show();
        Assets.get().music.get("bgm").play();

    }
    public void hide(){
        super.hide();
        Assets.get().music.get("bgm").stop();
    }

    @Override
    protected void initBg(Stage stage) {
        stage.addActor(new Image(Assets.get().drawable("ui/bg")));
    }

    @Override
    protected void initWorld(Stage stage) {
        world = new World(stage.getViewport(), stage.getBatch());
        stages.put(WORLD, world);
    }

    @Override
    protected void initUi(Stage stage) {

    }
}
