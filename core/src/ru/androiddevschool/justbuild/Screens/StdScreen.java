package ru.androiddevschool.justbuild.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import java.util.HashMap;

import ru.androiddevschool.justbuild.Utils.Names;
import ru.androiddevschool.justbuild.Utils.Values;

import static ru.androiddevschool.justbuild.Utils.Names.StageName.*;

/**
 * Created by 06k1102 on 30.03.2017.
 */
abstract class StdScreen implements Screen, Names, Values {
    protected HashMap<StageName, Stage> stages;
    protected InputMultiplexer multiplexer;

    public StdScreen(SpriteBatch batch) {
        stages = new HashMap<StageName, Stage>();
        stages.put(BG, initStage(batch));
        stages.put(WORLD, initStage(batch));
        stages.put(UI, initStage(batch));
        initBg(stage(BG));
        initWorld(stage(WORLD));
        initUi(stage(UI));
    }

    protected Stage stage(StageName name){ return stages.get(name); }

    private Stage initStage(SpriteBatch batch){
        OrthographicCamera camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(false);
        return new Stage(new StretchViewport(WORLD_WIDTH, WORLD_HEIGHT, camera), batch);
    }
    abstract protected void initBg(Stage stage);
    abstract protected void initWorld(Stage stage);
    abstract protected void initUi(Stage stage);

    @Override
    public void show() {
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(stage(UI));
        multiplexer.addProcessor(stage(WORLD));
        Gdx.input.setInputProcessor(multiplexer);
    }

    protected void act(float delta){
        stage(UI).act(delta);
        stage(WORLD).act(delta);
    }
    protected void postAct(){

    }
    protected void draw(){
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage(BG).draw();
        stage(WORLD).draw();
        stage(UI).draw();
    }

    @Override
    public void render(float delta) {
        act(delta);
        draw();
    }

    @Override
    public void resize(int width, int height) {
        stage(BG).getViewport().setScreenSize(width, height);
        stage(WORLD).getViewport().setScreenSize(width, height);
        stage(UI).getViewport().setScreenSize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        stage(BG).dispose();
        stage(WORLD).dispose();
        stage(UI).dispose();
    }
}