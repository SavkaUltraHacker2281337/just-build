package ru.androiddevschool.justbuild.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.io.File;
import java.util.HashMap;

/**
 * Created by 06k1102 on 30.03.2017.
 */
public class Assets {
    private static Assets ourInstance = new Assets();

    public static Assets get() {
        return ourInstance;
    }

    private Assets() {
        initImages();
        initFonts();
        initStyles();
        initSounds();
        for (String f : imgs.keySet()) System.out.println(f);
    }

    private void initSounds() {
        sounds = new HashMap<String, Sound>();
        sounds.put("hit", Gdx.audio.newSound(getHandle("music/hit.ogg")));
        sounds.put("die", Gdx.audio.newSound(getHandle("music/die.ogg")));
        sounds.put("lose", Gdx.audio.newSound(getHandle("music/lose.mp3")));
        music = new HashMap<String, Music>();
        music.put("bgm", Gdx.audio.newMusic(getHandle("music/bgm.mp3")));

    }

    private void initStyles() {
        btnStyles = new HashMap<String, Button.ButtonStyle>();
        btnStyles.put("long", new Button.ButtonStyle(drawable("ui/long-up"), drawable("ui/long-down"), null));
        btnStyles.put("small", new Button.ButtonStyle(drawable("ui/small-up"), drawable("ui/small-down"), null));
        btnStyles.put("long-text", new TextButton.TextButtonStyle(drawable("ui/long-up"), drawable("ui/long-down"), null, fonts.get("simple")));
        btnStyles.put("small-text", new TextButton.TextButtonStyle(drawable("ui/small-up"), drawable("ui/small-down"), null, fonts.get("simple")));
    }

    private void initFonts() {
        fonts = new HashMap<String, BitmapFont>();
        fonts.put("simple", new BitmapFont());
    }

    private void initImages() {
        imgs = new HashMap<String, TextureRegionDrawable>();
        //System.out.println("init");
        addFolderImg(getHandle("ui/"), "");
        addFolderImg(getHandle("imgs/"), "");
    }

    private void addFolderImg(FileHandle file, String prefix) {
        //System.out.println(file.path());
        if (file.isDirectory())
            for (FileHandle f : file.list())
                addFolderImg(f, prefix + file.name());
        else if (file.extension().equals("png") || file.extension().equals("jpg"))
            imgs.put(prefix + "/" + file.nameWithoutExtension(), makeDrawable(file));
    }

    private TextureRegionDrawable makeDrawable(FileHandle handle) {
        return new TextureRegionDrawable(new TextureRegion(new Texture(handle)));
    }

    private FileHandle getHandle(String fileName) {
        return Gdx.files.internal(fileName);
    }

    public TextureRegionDrawable drawable(String name) {
        return imgs.get(name);
    }

    public HashMap<String, TextureRegionDrawable> imgs;
    public HashMap<String, Button.ButtonStyle> btnStyles;
    public HashMap<String, BitmapFont> fonts;
    public HashMap<String, Sound> sounds;
    public HashMap<String, Music> music;
}