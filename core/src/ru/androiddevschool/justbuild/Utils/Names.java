package ru.androiddevschool.justbuild.Utils;

/**
 * Created by 06k1102 on 04.05.2017.
 */
public interface Names {
    enum StageName{
        BG,
        WORLD,
        UI
    }
    enum ScreenName{
        MENU,
        PLAY,
        LOSE
    }
}
