package ru.androiddevschool.justbuild.Model;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;


import ru.androiddevschool.justbuild.Utils.Values;

/**
 * Created by 06k1102 on 04.05.2017.
 */
public class Block extends Image {
    float time;
    float initX;
    float ampl;
    float speed;
    boolean moving;
    public Block(TextureRegionDrawable image){
        super(image);
        setAlign(Align.center);
        time = 0;
        this.speed = 1.5f;
        initX = Values.WORLD_WIDTH/2;
        ampl = Values.WORLD_WIDTH/2;
        moving = true;
    }

    public void act(float delta){
        super.act(delta);
        if (moving) {
            time += delta * speed;
            time %= 2 * Math.PI;
            setX((float) (initX + Math.sin(time) * ampl) - getWidth() / 2);
        }
    }

    public void stop(){ moving = false; }

    public void play(){ moving = true; }

    public void drop(Block previous){
        if (previous.hasActions()) return;
        if (getX() < previous.getRight() && getRight() > previous.getX()){
            stop();
            addAction(Actions.moveTo(getX(),previous.getTop(), 0.5f));
        }else{
            play();
        }
    }
}
