package ru.androiddevschool.justbuild.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by 06k1102 on 06.04.2017.
 */
public class LoseScreen extends StdScreen {
    public LoseScreen(SpriteBatch batch) {
        super(batch);
    }

    @Override
    protected void initBg(Stage stage) {

    }

    @Override
    protected void initWorld(Stage stage) {

    }

    @Override
    protected void initUi(Stage stage) {

    }
}
