package ru.androiddevschool.justbuild;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.HashMap;

import ru.androiddevschool.justbuild.Screens.Menu;
import ru.androiddevschool.justbuild.Screens.Play;
import ru.androiddevschool.justbuild.Utils.Assets;
import ru.androiddevschool.justbuild.Utils.Names;

import static ru.androiddevschool.justbuild.Utils.Names.ScreenName.*;

public class JustBuild extends Game implements Names {

	public static JustBuild instance = new JustBuild();
	private JustBuild(){}
	public static JustBuild get(){return instance;}

	public static JustBuild getInstance() {
		return instance;
	}

	@Override
	public void create () {
		Assets.get();
		batch = new SpriteBatch();
		screens = new HashMap<ScreenName, Screen>();
		screens.put(MENU, new Menu(batch));
		screens.put(PLAY, new Play(batch));
		setScreen(MENU);
	}

	public void setScreen(ScreenName name) {
		if (screens.containsKey(name))
			setScreen(screens.get(name));
	}
	private SpriteBatch batch;
	private HashMap<ScreenName, Screen> screens;
}